package com.deichmann.trafficjam.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Vehicle {

    private String symbol;
    private String description;

    public Vehicle generateClone(){
        return new Vehicle(this.symbol, this.description);
    }

    public void reverseDescription(){
        var charArray = this.description.toCharArray();
        var newCharArray = new char[charArray.length];
        for (int i = charArray.length - 1; i > -1; i--) {
            newCharArray[charArray.length - i - 1] = charArray[i];
        }
        this.description = new String(newCharArray);
    }
}
