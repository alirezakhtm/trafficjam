package com.deichmann.trafficjam.model;

/** Bus Model. */
public class Bus extends Vehicle {

    public Bus(){
        super("B", "-/OOOO\\");
    }
}
