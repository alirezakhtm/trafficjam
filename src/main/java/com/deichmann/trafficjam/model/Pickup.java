package com.deichmann.trafficjam.model;

/** Pickup Model. */
public class Pickup extends Vehicle {

    public Pickup() {
        super("P", "/O\\__");
    }
}
