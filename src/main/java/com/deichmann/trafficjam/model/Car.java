package com.deichmann.trafficjam.model;

/** Car Model. */
public class Car extends Vehicle {

    public Car() {
        super("C", "/OO\\");
    }
}
