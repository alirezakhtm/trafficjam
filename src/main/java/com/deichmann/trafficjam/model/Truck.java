package com.deichmann.trafficjam.model;

import lombok.Data;

@Data
public class Truck extends Vehicle {

    private boolean truckIsFull = false;

    public Truck() {
        super("T", "/O|___");
    }

    @Override
    public Vehicle generateClone() {
        var truck = new Truck();
        truck.setSymbol(this.getSymbol());
        truck.setDescription(this.getDescription());
        truck.setTruckIsFull(this.isTruckIsFull());
        return truck;
    }

    public void fullTrack(){
        this.setDescription("/O|###");
        this.truckIsFull = true;
    }


}
