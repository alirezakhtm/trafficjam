package com.deichmann.trafficjam.model;

import com.deichmann.trafficjam.model.*;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

/** TrafficJam Model. */
public class TrafficJam {

	private final static Map<String, Vehicle> vehicles = Map.of(
			"C", new Car(),
			"B", new Bus(),
			"P", new Pickup(),
			"T", new Truck()
	);

	@Getter
	private LinkedList<Vehicle> vehicleList = new LinkedList<>();

	public TrafficJam(String code) {
		validateInput(code);
		vehicleList = Arrays.stream(code.toUpperCase().split("|"))
				.map(s -> vehicles.get(s).generateClone()).collect(Collectors.toCollection(LinkedList::new));
	}

	public String print() {
		return generateStringFromDescriptions(vehicleList);
	}

	public void turnToHead() {
		if(!vehicleList.isEmpty()){
			vehicleList.forEach(Vehicle::reverseDescription);
			reverseVehicleList();
		}
	}

	public void removeFirst() {
		vehicleList.removeFirst();
	}

	public void removeLast() {
		vehicleList.removeLast();
	}

	public boolean fillTrucks() {
		Optional<Vehicle> truck = vehicleList.stream()
				.filter(vehicle -> vehicle instanceof Truck)
				.filter(vehicle -> !((Truck) vehicle).isTruckIsFull())
				.findFirst();
		if(truck.isEmpty()) return false;
		((Truck) truck.get()).fullTrack();
		return true;
	}

	private void removeLastSeparator(StringBuilder sb) {
		if (!sb.isEmpty()) {
			sb.deleteCharAt(sb.length() - 1);
			sb.deleteCharAt(sb.length() - 1);
		}
	}

	private String generateStringFromDescriptions(List<Vehicle> lst) {
		StringBuilder sb = new StringBuilder();
		lst.forEach(vehicle -> {
			sb.append(vehicle.getDescription());
			sb.append("  ");
		});
		removeLastSeparator(sb);
		return sb.toString();
	}

	private void reverseVehicleList() {
		var rst = new LinkedList<Vehicle>();
		for (int i = vehicleList.size() - 1; i > -1; i--) {
			rst.add(vehicleList.get(i));
		}
		vehicleList = rst;
	}

	private void validateInput(String code) {
		Arrays.stream(code.toUpperCase().split("|")).forEach(s -> {
			if(!vehicles.containsKey(s)) throw new IllegalArgumentException("Input Code is not Valid.");
		});
	}
}
