package com.deichmann.trafficjam.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.LinkedList;

@Data
@AllArgsConstructor
public class TrafficJamResponse {

    private LinkedList<Vehicle> vehicleList;
    private String descriptions;

}
