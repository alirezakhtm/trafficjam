package com.deichmann.trafficjam.controller;

import com.deichmann.trafficjam.model.TrafficJamResponse;
import com.deichmann.trafficjam.service.TrafficJamInterface;
import jakarta.websocket.server.PathParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class TrafficController {

    private final TrafficJamInterface trafficJamService;

    @GetMapping("/api/traffic-description")
    public TrafficJamResponse getDescription(@RequestParam("traffic") String traffic) {
        return trafficJamService.getDescription(traffic);
    }

    @GetMapping("/api/traffic-description/turn-to-head")
    public TrafficJamResponse getTurnToHeadDescription(@RequestParam("traffic") String traffic){
        return trafficJamService.getTurnToHeadDescription(traffic);
    }

    @GetMapping("/api/traffic-description/remove-first")
    public TrafficJamResponse getDescriptionWithRemoveFirst(@RequestParam("traffic") String traffic){
        return trafficJamService.getDescriptionWithRemoveFirst(traffic);
    }

    @GetMapping("/api/traffic-description/remove-last")
    public TrafficJamResponse getDescriptionWithRemoveLast(@RequestParam("traffic") String traffic){
        return trafficJamService.getDescriptionWithRemoveLast(traffic);
    }

    @GetMapping("/api/traffic-description/fill-trucks")
    public TrafficJamResponse getDescriptionAndFillTrucks(@RequestParam("traffic") String traffic,
                                                          @RequestParam("num") int fillTrucksNumber){
        return trafficJamService.getDescriptionAndFillTrucks(traffic, fillTrucksNumber);
    }

}
