package com.deichmann.trafficjam.service;

import com.deichmann.trafficjam.model.TrafficJamResponse;

/** Interface of TrafficJamService */
public interface TrafficJamInterface {

    TrafficJamResponse getDescription(String traffic);
    TrafficJamResponse getTurnToHeadDescription(String traffic);
    TrafficJamResponse getDescriptionWithRemoveFirst(String traffic);
    TrafficJamResponse getDescriptionWithRemoveLast(String traffic);
    TrafficJamResponse getDescriptionAndFillTrucks(String traffic, int numberOfFillTrucks);

}
