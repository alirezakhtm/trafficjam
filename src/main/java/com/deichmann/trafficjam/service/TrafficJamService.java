package com.deichmann.trafficjam.service;

import com.deichmann.trafficjam.model.TrafficJam;
import com.deichmann.trafficjam.model.TrafficJamResponse;
import org.springframework.stereotype.Service;

/**
 * this service handles TrafficJam model and manipulates it.
 * */
@Service
public class TrafficJamService implements TrafficJamInterface{

    /**
     * this function describes traffic statement
     * @param traffic traffic statement.
     * */
    @Override
    public TrafficJamResponse getDescription(String traffic) {
        var trafficJam = new TrafficJam(traffic);
        return new TrafficJamResponse(trafficJam.getVehicleList(), trafficJam.print());
    }

    /**
     * this function turn to head and return description according to this change.
     * @param traffic traffic statement.
     * */
    @Override
    public TrafficJamResponse getTurnToHeadDescription(String traffic) {
        var trafficJam = new TrafficJam(traffic);
        trafficJam.turnToHead();
        return new TrafficJamResponse(trafficJam.getVehicleList(), trafficJam.print());
    }

    /**
     * this function remove first element from description of traffic.
     * @param traffic traffic statement.
     * */
    @Override
    public TrafficJamResponse getDescriptionWithRemoveFirst(String traffic) {
        var trafficJam = new TrafficJam(traffic);
        trafficJam.removeFirst();
        return new TrafficJamResponse(trafficJam.getVehicleList(), trafficJam.print());
    }

    /**
     * this function remove last element from description of traffic.
     * @param traffic traffic statement.
     * */
    @Override
    public TrafficJamResponse getDescriptionWithRemoveLast(String traffic) {
        var trafficJam = new TrafficJam(traffic);
        trafficJam.removeLast();
        return new TrafficJamResponse(trafficJam.getVehicleList(), trafficJam.print());
    }

    /**
     * this function fills trucks, that exist in description.
     * @param traffic traffic statement.
     * @param numberOfFillTrucks  The order of calling the function.
     * */
    @Override
    public TrafficJamResponse getDescriptionAndFillTrucks(String traffic, int numberOfFillTrucks) {
        var trafficJam = new TrafficJam(traffic);
        for (int i = 0; i < numberOfFillTrucks; i++) {
            trafficJam.fillTrucks();
        }
        return new TrafficJamResponse(trafficJam.getVehicleList(), trafficJam.print());
    }


}
