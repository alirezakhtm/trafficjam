package com.deichmann.trafficjam;

import com.deichmann.trafficjam.model.TrafficJamResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class HttpRequestTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    public void getDescriptionTest() {
        this.callApiAndCompareExpectedResponse(
                "/api/traffic-description?traffic=CPCB",
                "/OO\\  /O\\__  /OO\\  -/OOOO\\");
    }

    @Test
    public void getTurnToHeadDescriptionTest() {
        this.callApiAndCompareExpectedResponse(
                "/api/traffic-description/turn-to-head?traffic=CPCB",
                "\\OOOO/-  \\OO/  __\\O/  \\OO/");
    }

    @Test
    public void getDescriptionWithRemoveFirstTest() {
        this.callApiAndCompareExpectedResponse(
                "/api/traffic-description/remove-first?traffic=CPCBC",
                "/O\\__  /OO\\  -/OOOO\\  /OO\\"
        );
    }

    @Test
    public void getDescriptionWithRemoveLastTest() {
        this.callApiAndCompareExpectedResponse(
                "/api/traffic-description/remove-last?traffic=PCBC",
                "/O\\__  /OO\\  -/OOOO\\"
        );
    }

    @Test
    public void getDescriptionAndFillTrucksTest(){
        this.callApiAndCompareExpectedResponse(
                "/api/traffic-description/fill-trucks?traffic=TTT&num=1",
                "/O|###  /O|___  /O|___"
        );
    }

    @Test
    public void getDescriptionAndFillTrucksTest2(){
        this.callApiAndCompareExpectedResponse(
                "/api/traffic-description/fill-trucks?traffic=TTT&num=2",
                "/O|###  /O|###  /O|___"
        );
    }

    private TrafficJamResponse callGetApi(String address) {
        return this.restTemplate.getForObject("http://localhost:" + port + address,
                TrafficJamResponse.class);
    }

    private void callApiAndCompareExpectedResponse(String address, String expectedResponse) {
        var rsp = this.callGetApi(address);
        assertEquals(rsp.getDescriptions(), expectedResponse);
    }

}
